import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class ActionsPage {
    public PromoPage logOut() {
        $(".user__icon").click();
        $(byText("Выйти")).click();
        return page(PromoPage.class);
    }

    public SelenideElement getHeaderRight(){
        return $(".header2__right").shouldBe(visible);
    }


}
